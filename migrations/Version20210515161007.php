<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210515161007 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE app_task (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(200) NOT NULL, description VARCHAR(800) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_todo (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(200) NOT NULL, description VARCHAR(800) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE todo_task (todo_id INT NOT NULL, task_id INT NOT NULL, INDEX IDX_DAFBD3AEA1EBC33 (todo_id), INDEX IDX_DAFBD3A8DB60186 (task_id), PRIMARY KEY(todo_id, task_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE todo_task ADD CONSTRAINT FK_DAFBD3AEA1EBC33 FOREIGN KEY (todo_id) REFERENCES app_todo (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE todo_task ADD CONSTRAINT FK_DAFBD3A8DB60186 FOREIGN KEY (task_id) REFERENCES app_task (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE todo_task DROP FOREIGN KEY FK_DAFBD3A8DB60186');
        $this->addSql('ALTER TABLE todo_task DROP FOREIGN KEY FK_DAFBD3AEA1EBC33');
        $this->addSql('DROP TABLE app_task');
        $this->addSql('DROP TABLE app_todo');
        $this->addSql('DROP TABLE todo_task');
    }
}
