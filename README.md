# ToloDolo - API Documentation

### This API works with the JSON format. Requests must be sent in JSON format and responses are sent in JSON format.

## Endpoints

* /todos (GET)
* /todos (POST)
* /todos/{id} (GET)
* /todos/{id} (PUT) 
* /todos/{id} (DELETE) 
- /tasks (POST)
- /tasks/{id} (DELETE)

## Operations
### Get list of todos
`Returns a list of all Todos in Json format`
* RequestType: GET
* URL: /todos
* ReturnType: JSON

### Create todo 
`Expects a Todo without id and returns a Todo with id`
* RequestType: POST
* URL: /todos
* ReturnType: JSON
* Example:
```html
{
    "name": "Learn Symfony",
    "description": "Understand Symfony Framework",
    "tasks": [12]
}

```

### Get todo
`Expects an ID and returns the matching todo`
* RequestType: GET
* URL: /todos/{id}
* ReturnType: JSON

### Update todo 
`Expects a Todo with ID and Overwrites the matching todo`
* RequestType: PUT
* URL: /todos/{id}
* ReturnType: JSON
* Example:
```html
{
    "name": "Learn Symfony",
    "description": "Understand Symfony Framework and FriendsOfSymfony",
    "tasks": [1, 3]
}

```

### Delete todo
`Expects an ID and deletes the matching todo`
* RequestType: DELETE
* URL: /todos/{id}

### Create task
`Expects a Task without id and returns a Task with id`
* RequestType: POST
* URL: /tasks
* ReturnType: JSON
* Example:
```html
{
    "name": "Read Docs",
    "description": "Task Description"
}

```

### Delete task
`Expects an ID and deletes the matching task`
* RequestType: DELETE
* URL: /tasks/{id}

## Data structure Todo

```html
{
    id [mandatory] 
    name [mandatory] 
    description 
    tasks: [ 
        { 
            id [mandatory] 
            name [mandatory] 
            description 
        } 
    ]
}

```

## Resources

* [Symfony](https://symfony.com/)
* [FOSRestBundle](https://symfony.com/doc/current/bundles/FOSRestBundle/index.html)
* [Doctrine](https://symfony.com/doc/current/doctrine.html)
