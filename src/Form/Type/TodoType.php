<?php


namespace App\Form\Type;

use App\Entity\Task;
use App\Entity\Todo;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

class TodoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotNull(),
                    new Length([
                        'max' => 200,
                    ])
                ]
            ])
            ->add('description', TextType::class, [
                'empty_data'  => '',
                'constraints' => [
                    new Length([
                        'max' => 800,
                    ])
                ]
            ])
            ->add('tasks', EntityType::class, [
                'class' => Task::class,
                'multiple' => true,
                'constraints' => [
                    new NotNull(),
                ]
            ])
        ;}

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Todo::class,
        ]);
    }
}