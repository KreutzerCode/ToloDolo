<?php


namespace App\Controller;


use App\Entity\Todo;
use App\Form\Type\TodoType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TodoController extends AbstractApiController
{
    public function indexAction(Request $request): Response
    {
        $todos = $this->getDoctrine()->getRepository(Todo::class)->findAll();

        return $this->respond($todos);
    }

    public function showAction(Request $request): Response {
        $todoId = $request->get('id');
        $todo = $this->getDoctrine()->getRepository(Todo::class)->findOneBy(['id' => $todoId]);

        if(!$todo) {
            throw new NotFoundHttpException('Todo not exist!');
        }

        return $this->respond($todo);
    }

    public function deleteAction(Request $request): Response {
        $todoId = $request->get('todoId');

        /** @var Todo $todo */
        $todo = $this->getDoctrine()->getRepository(Todo::class)->findOneBy([
            'id' => $todoId,
        ]);

        if(!$todo) {
            throw new NotFoundHttpException('Todo not found');
        }

        // remove child tasks
        if($todo->getTasks()->count() > 0) {
            foreach ($todo->getTasks() as $task) {
                $this->getDoctrine()->getManager()->remove($task);
            }
        }

        $this->getDoctrine()->getManager()->remove($todo);
        $this->getDoctrine()->getManager()->flush();

        return $this->respond(null);
    }

    public function createAction(Request $request): Response
    {
        $form = $this->buildForm(TodoType::class);

        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var Todo $todo */
        $todo = $form->getData();

        $this->getDoctrine()->getManager()->persist($todo);
        $this->getDoctrine()->getManager()->flush();

        return $this->respond($todo);
    }

    public function updateAction(Request $request): Response {
        $todoId = $request->get('todoId');

        $todo = $this->getDoctrine()->getRepository(Todo::class)->findOneBy(['id' => $todoId]);

        if(!$todo) {
            throw new NotFoundHttpException('Todo not Found');
        }

        $form = $this->buildForm(TodoType::class, $todo, [
            'method' => $request->getMethod(),
        ]);

        $form->handleRequest($request);

        if(!$form->isSubmitted() || !$form->isValid()) {
            // throw error
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var Todo $cart */
        $todo = $form->getData();

        $this->getDoctrine()->getManager()->persist($todo);
        $this->getDoctrine()->getManager()->flush();

        return $this->respond($todo);
    }
}