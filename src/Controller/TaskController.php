<?php


namespace App\Controller;


use App\Entity\Task;
use App\Form\Type\TaskType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TaskController extends AbstractApiController
{

    public function createAction(Request $request): Response
    {
        $form = $this->buildForm(TaskType::class);

        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var Task $task */
        $task = $form->getData();

        $this->getDoctrine()->getManager()->persist($task);
        $this->getDoctrine()->getManager()->flush();

        return $this->respond($task);
    }

    public function deleteAction(Request $request): Response {
        $taskId = $request->get('taskId');

        /** @var Task $task */
        $task = $this->getDoctrine()->getRepository(Task::class)->findOneBy([
            'id' => $taskId,
        ]);

        if(!$task) {
            throw new NotFoundHttpException('Task not found');
        }

        $this->getDoctrine()->getManager()->remove($task);
        $this->getDoctrine()->getManager()->flush();

        return $this->respond(null);
    }
}